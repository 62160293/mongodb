const ROLE = {
  ADMIN: 'LOCAL_ADMIN',
  USER: 'USER'
}
module.exports = {
  ROLE
}
